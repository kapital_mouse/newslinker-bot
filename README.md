This is a small repository containing the source for DragonNewsLinker, which allows you to specify certain channels to watch for links. When a link is found, it'll post it to a second, archival channel.

The message to do this is `!newslink <#channel id>`. 

To run the bot, you'll need docker and docker-compose. Once those are installed, edit the `docker.compose.yml` file, replacing the DISCORD_TOKEN with a TOKEN you control from your own bot.

`docker-compose build && docker-compose up`

You can backup the current watchlist of files by making a standard redis backup from the data stored in the docker volume newslinker_redisdata.
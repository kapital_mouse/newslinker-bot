FROM python:3.6-alpine

CMD python -m newslinker.main
WORKDIR /app

RUN apk add --virtual build-dependencies build-base gcc

ADD requirements.txt /app/requirements.txt

RUN pip install -U pip && pip install -r /app/requirements.txt

ADD newslinker /app/newslinker

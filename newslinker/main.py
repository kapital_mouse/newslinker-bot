import os
from functools import lru_cache

import discord
from aioredis import create_redis, Redis
from discord import Channel, Message
from discord.ext.commands import Bot, Context, check

bot: Bot = Bot(command_prefix='!')
redis: Redis = None


@lru_cache(100)
def find_mod_roles(roles):
    return discord.utils.find(lambda r: r.name.lower() in ['mod', 'admin'], roles)


def has_admin(server: discord.Server):
    return find_mod_roles(tuple(server.roles)) is not None


def is_admin(user: discord.Member):
    return user.server.owner == user or find_mod_roles(tuple(user.roles)) is not None


def require_admin(ctx: Context):
    # no server => is a DM
    return ctx.message.server is None or not has_admin(ctx.message.server) or is_admin(ctx.message.author)


async def reposting(channel_id):
    return bool(await redis.hlen(channel_id))


@bot.event
async def on_ready():
    global redis
    redis = await create_redis('redis://redis:6379', loop=bot.loop)

    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.command()
@check(require_admin)
async def ping():
    await bot.say('pong')


@bot.command(pass_context=True)
@check(require_admin)
async def newslink(ctx, dest: str):
    # this is here rather than in the param list because discord.py does some parameter conversion magic that breaks
    ctx: Context = ctx

    # turn off reposting
    if await redis.hget(ctx.message.id, dest) is not None:
        await bot.say('no longer reposting to <#{}>'.format(dest))
        await redis.hdel(ctx.message.id, dest)
    # attempt to enable reposting
    else:
        dest_channel: Channel = bot.get_channel(dest)
        if dest_channel is None or dest_channel.is_private or dest_channel.server != ctx.message.server:
            await bot.say('sorry I don\'t know that channel ID')
        elif not dest_channel.permissions_for(ctx.message.server.get_member(bot.user.id)).send_messages:
            await bot.say('sorry I can\'t post to that channel')
        elif dest_channel == ctx.message.channel.id:
            await bot.say('yeah, no infinite links kiddo. _makes a small bellow of fire_')
        else:
            await redis.hset(ctx.message.channel.id, dest, 1)
            await bot.say('got it! reposting links from this channel to <#{}>'.format(dest))


@bot.event
async def on_message(message: Message):
    await bot.process_commands(message)
    if ('http://' in message.content or 'https://' in message.content) and await reposting(message.channel.id):
        embed = discord.Embed(title='From {}'.format(message.channel.name), color=0xcd1ff8)
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url or message.author.default_avatar_url)

        for repost_to in await redis.hgetall(message.channel.id):
            await bot.send_message(bot.get_channel(repost_to.decode()), content=message.content, embed=embed)


print('installed commands:', bot.commands)

if __name__ == '__main__':
    try:
        bot.run(os.getenv('DISCORD_TOKEN'))
    except StopIteration:
        def cleanup():
            yield from bot.close()
            if redis is not None:
                redis.close()
        cleanup()
